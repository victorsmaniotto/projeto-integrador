from selenium import webdriver
import os
from time import sleep
from selenium.webdriver.common.keys import Keys


dir_path = os.getcwd()
# O caminho do chromedriver
chromedriver = os.path.join(dir_path, "chromedriver")
# Caminho onde será criada pasta profile
profile = os.path.join(dir_path, "profile", "wpp")


options = webdriver.ChromeOptions()
# Configurando a pasta profile, para mantermos os dados da seção
options.add_argument(r"user-data-dir={}".format(profile))
# Inicializa o webdriver
driver = webdriver.Chrome(chromedriver, chrome_options=options)
# Abre o whatsappweb
driver.get("https://accounts.google.com/AccountChooser/identifier?service=mail&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&flowName=GlifWebSignIn&flowEntry=AccountChooser")
