import json
import os
import requests
from bs4 import BeautifulSoup

url = "https://www.vagas.com.br/vagas-de-programador"
data_dir = "bdvagas"
response = requests.get(url)
response.text
soup = BeautifulSoup(response.text)
lista = []
for vagas in soup.find_all("div", attrs={"class": "informacoes-header"}):
    # for vagas in soup.find_all("ul", attrs={"class": "facet facetValues js-facet-ul"}):
    titulo = (vagas.h2.a['title'])
    link = (vagas.h2.a['href'])
   # print('titulo da vaga: {}' .format(titulo))
    #print('link da vaga :{}' .format(link))
   # print('-'*110)
    lista.append([titulo, link])
