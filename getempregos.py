import infojobs
import indeed
import trabalhabrasil
import vagas

import csv


with open('getempregos.csv', 'w', newline='') as arquivo:
    final = csv.writer(arquivo, delimiter=';')
    final.writerow(["titulo", "link"])

    for x in infojobs.lista:

        final.writerow(x)

    for x in indeed.lista:

        final.writerow(x)

    for x in trabalhabrasil.lista:

        final.writerow(x)

    for x in vagas.lista:

        final.writerow(x)
