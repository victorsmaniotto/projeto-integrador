
import requests
from bs4 import BeautifulSoup


#vaga = input('digite uma vaga de seu interesse:')
#estado = 64

for x in range(10):
    pesquisa = 'https://www.infojobs.com.br/vagas-de-emprego-programador.aspx?Page={}'.format(
        x+1)

    req = requests.get(pesquisa)
    html = req.text

    soup = BeautifulSoup(html, 'html.parser')
    bloco_vagas = soup.find_all('div', {'class': 'vaga'})

lista = []
for link in bloco_vagas:
    titulo_vaga = (link.a['title'])
    link_vaga = (link.a['href'])
    #print('titulo da vaga: {}' .format(titulo_vaga))
   # print('link da vaga :{}' .format(link_vaga))
    # print('-'*110)
    lista.append([titulo_vaga, link_vaga])
